package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/joho/godotenv/autoload"
	. "gophra/helper"
	"os"
)

var DB *gorm.DB

type DbCfg struct {
	Host string
	Port string
	User string
	Password string
	DbName string
}


func DbConnect(){
	var err error

	cfg := DbCfg{
		Host: GetEnv("DB_HOST", "localhost"),
		Port: GetEnv("DB_PORT", "5433"),
		User: GetEnv("DB_USERNAME", "gophra"),
		Password: GetEnv("DB_PASSWORD", "secret"),
		DbName: GetEnv("DB_DATABASE", "gophra"),
	}

	args := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.DbName)
	DB, err = gorm.Open("postgres", args)
	//DB, err = gorm.Open("postgres", "host=127.0.0.1 port=5433 user=gophra dbname=gophra password=secret sslmode=disable")
	CheckErr(err)
}

func GetEnv(key string, defaultValue ...string) string {
	val, exist := os.LookupEnv(key)
	if !exist {
		if len(defaultValue)>0 {
			return defaultValue[0]
		} else {
			return ""
		}
	}

	return val
}