package models

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
    "github.com/valyala/fasthttp"
    "golang.org/x/crypto/bcrypt"
    . "gophra/db"
	"gophra/helper"
	"gophra/types"
    "log"
)
type User struct {
	ID        uint `gorm:"primary_key"`
	Name         string `gorm:"type:varchar(100)",json:"name"`
	//Age          sql.NullInt64
	//Birthday     *time.Time
	Email        string  `gorm:"type:varchar(100);unique_index",json:"email"`
	Password	string `json:"-" string:"-"`
	Token		string `json:"-"`
	Active		bool `gorm:"default:true" json:"active"`
	CreatedAt	string `gorm:"-"`
	UpdatedAt	string `gorm:"-"`
    //Role         string  `gorm:"size:255"` // set field size to 255
    //MemberNumber *string `gorm:"unique;not null"` // set member number to unique and not null
    //Num          int     `gorm:"AUTO_INCREMENT"` // set num to auto incrementable
    //Address      string  `gorm:"index:addr"` // create index with name `addr` for address
    //IgnoreMe     int     `gorm:"-"` // ignore this field
}

// Выполняет авторизацию пользователя
func (u *User) Authenticate(data types.MapSs) interface{}  {

	// сначала достаем пользователя по email
	err := DB.Where("email = ?", data["email"]).First(u)
	if err.Error == gorm.ErrRecordNotFound {
		//fmt.Printf("\nERROR: %s\n", err.GetErrors())
		return false
	}

	// user найден, теперь проверяем пароль
	if ComparePassword(data["password"], u.Password) {

		// генерим токен - строка из случайных символов
		token := helper.RandomString(50)
		// сохраняем токен
		u.Token = string(token)
		DB.Save(u)

		return u.Token
	}
	return false
}

// Проверяет "авторизованность" пользователя по token
// присваивает пользователя в объект модели
func (u *User) Authenticated(token string)  error{

	err := DB.Where("token = ?", token).First(u)
	if err.Error == gorm.ErrRecordNotFound {
		fmt.Printf("\nERROR: %s\n", err.GetErrors())
		return errors.New("User not found")
	}
	return nil
}

// достает и возвращает из объекта запроса объект авторизованного пользователя
// объект пользователя попадает в объект запроса в методе authMiddleware
func (u *User) GetAuthUser(ctx *fasthttp.RequestCtx) bool {

    user,ok := ctx.UserValue("authUser").(User)
    *u = user
    return ok

}

// сравнивает парль текстом с хешем
func ComparePassword(plainPwd string, hashedPwd string) bool  {

    err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd))
    if err != nil {
        log.Println(err)
        return false
    }

    return true
}