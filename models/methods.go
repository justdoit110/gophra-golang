package models

import (
    "fmt"
    . "gophra/db"
)

// общие методы которые можно подключить к любой модели
type DbModel struct {}

// достает по id
func (obj *DbModel) GetById(id uint, dest interface{}) error {

    err := DB.First(dest, id)
    if err != nil {
        return err.Error
    }
    return nil
}

func GetUserCategories(userId uint) ([]*Category, error)  {

    var categories []*Category
    res := DB.Preload("Lists", "status != 0" ).Find(&categories, "user_id = ? and status != 0", fmt.Sprint(userId))
    if res.Error != nil {
        return nil, res.Error
    }
    return categories, nil
}