package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"gophra/models"
	"gophra/helper"
	. "gophra/db"
	. "gophra/types"
)


/**
handler запроса на регистрацию нового пользователя
 */
func Register(ctx *fasthttp.RequestCtx) {
	// подготовка ответа
	responseData := &ResponseData{ctx,make(MapSs),200}

	var data = &RegisterReq{}
	// сущность пользователя, объект модели
	var user = &models.User{}

	err := json.Unmarshal(ctx.PostBody(), data)
	helper.CheckErr(err)

	// валидность данных
	if errs := data.ValidateMk(); len(errs)>0 {
		responseData.SetData(map[string]interface{}{"error": errs}, fasthttp.StatusBadRequest)
		helper.Response(responseData)
		return
	}

	// копируем поля из валидированного объекта запроса в объект модели
	user.Email = data.Email
	user.Name = data.Name
	user.Password = helper.PwdHash(data.Password)
	// пишем в БД
	if res := DB.Create(&user).GetErrors(); len(res)>0 {
		fmt.Printf("query res: %v", res)
		responseData.SetData(MapSs{"error": "Internal Server Error"}, fasthttp.StatusInternalServerError)
		helper.Response(responseData)
		return
	}

	responseData.Data = MapSs{"success": "registered"}
	helper.Response(responseData)
}
