Простой мини-сервис который позволяет создавать списки "фраз и переводов"  
для дальнейшего удобного изучения/повторения.
 
![](demo.gif)


Без фреймворка, чисто API бэкенд. Фронт на React [здесь](https://bitbucket.org/justdoit110/gophra_client).  
Вместо нативного `net/http` используется 
[fasthttp](https://github.com/valyala/fasthttp) + [router](https://github.com/buaazp/fasthttprouter),
DB Postgres + [orm](https://github.com/jinzhu/gorm).  
Компиляция, build и запуск выполняется в докере. БД тоже в докере.

  
#####Краткое описание/док-ия методов

__Регистрация__  
`POST /api/register`
```json
{
    "name": "string",
    "email": "string,email,required",
    "password": "string,required"
}
```
  
__Авторизация__  
`POST /api/login`
```json
{
    "email": "string,email,required",
    "password": "string,required"
}
```

__Добавить категорию__  
`POST /api/category`
```json
{
    "title": "string title",
    "token": "authToken"
}
```

__Посмотреть категорию__  
```
GET /api/category/:id
Headers:
Authorization: Bearer {authToken}
```

__Добавить Список (list)__  
`POST /api/list`
```json
{
    "title": "string title",
    "categoryId": int,
    "token": "authToken"
}
```
__Добавить "пачку" фраз в список__  
Фраза и её перевод должны быть разделены переносом строки (\n)  
`POST /api/list/:listId/phrases`
```json
{
    "phrases": "phrase1\ntranslate1\nphrase2\ntranslate2\n..."
}
```
```
Headers:
Authorization: Bearer {authToken}
```
__Получить фразы списка__  
`GET /api/list/:listId/phrases`
```
Headers:
Authorization: Bearer {authToken}
```

Запуск проекта при разработке с хотрелоад  
`gin -a 8080 run main.go`
