package main

import (
	"flag"
	"fmt"
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"gophra/db"
	"gophra/handlers"
	. "gophra/types"
	"log"
	"strconv"
)


func main() {
	var app_port int

	router := fasthttprouter.New()

	// обработка Options запросов
	router.OPTIONS("/*path", OptionReqHandler)
	// регистрация
	router.POST("/api/register", handlers.Register)
	// авторизация
	router.POST("/api/login", handlers.Login)

	// описание "закрытых" API роутов
	router = RouteGroupCompose(router, func() []RgRoute {
		// указываем capacity = кол-ву роутов (optional)
		routes := make([]RgRoute, 0, 10)
		// path-префикс
		prefix := "/api"
		// добавить/создать категорию
		routes = append(routes, RgRoute{Method: "POST", Path: prefix+"/category", Handler: handlers.CategoryStore})
		// "посмотреть" категорию
		routes = append(routes, RgRoute{Method: "GET", Path: prefix+"/category/:categoryId", Handler: handlers.CategoryShow})
		// получить Списки в категории
		routes = append(routes, RgRoute{Method: "GET", Path: prefix+"/category/:categoryId/lists", Handler: handlers.CategoryShowLists})

		// добавить список(list) в категорию(category)
        routes = append(routes, RgRoute{Method: "POST", Path: prefix+"/list", Handler: handlers.ListStore})
        // читаем/смотрим "list"
        routes = append(routes, RgRoute{Method: "GET", Path: prefix+"/list/:listId", Handler: handlers.ListShow})
		// добавить фразу в список
		routes = append(routes, RgRoute{Method: "POST", Path: prefix+"/list/:listId/phrase", Handler: handlers.PhraseStore})
		// добавить "пачку" фраза-перевод
		routes = append(routes, RgRoute{Method: "POST", Path: prefix+"/list/:listId/phrases", Handler: handlers.PhrasesStore})
		// достаем все фразы из списка
		routes = append(routes, RgRoute{Method: "GET", Path: prefix+"/list/:listId/phrases", Handler: handlers.ListShowPhrases})
		// получить древовидную структуру Категории->Списки (для формирования меню на фронте)
		routes = append(routes, RgRoute{Method: "GET", Path: prefix+"/menu", Handler: handlers.UserMenu})

		return routes
	}, MiddlewareCompose(LogMiddleware, AuthMiddleware))


	db.DbConnect()

	flag.IntVar(&app_port, "app_port", 8080, "application port")
	flag.Parse()
	addr := "0.0.0.0:"+strconv.Itoa(app_port)

	fmt.Printf("Started: http://%s\n", addr)
	log.Fatal(fasthttp.ListenAndServe(addr, router.Handler))
}

// с помощью данной ф-и можно создать некоторую группу
// роутов со своим [блэк джеком и...] path-префиксом и композицией middlewares
// внутри ф-и group указывается префикс, создается и возвращается slice RgRoute
func RouteGroupCompose(router FHRouter, group func() []RgRoute, middlewareCompose ...MiddlewareFunc) FHRouter  {

	routes := group()

	// сделаем пустой middleware
	middleware := func(handle HandleFunc ) HandleFunc {return handle}
	// (@todo возможно это можно сделать как-то по другому, чтобы не шаманить с пустой ф-ей)
	// Но если мы передали middleware при вызове ф-и, то будем использовать его
	if len(middlewareCompose)>0 {
		middleware = middlewareCompose[0]
	}

	for _, item := range routes {
		// инициализация метод+роут+mw(handler)
		router.Handle(item.Method, item.Path, middleware(item.Handler) )
	}

	return router

}

