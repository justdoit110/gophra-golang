package main

import (
    "encoding/json"
    "github.com/valyala/fasthttp"
    "gophra/helper"
    "gophra/models"
    "strings"
    . "gophra/types"

)

// Auth middleware
func AuthMiddleware(next HandleFunc) HandleFunc{

    return func(ctx *fasthttp.RequestCtx){
        //fmt.Println("AUTH MW")
        var request map[string]interface{}
        var user models.User
        var err error
        token := ""

        // сначала ищем токен в теле запроса @todo возможно следует оставить один вариант передачи токена (см. ниже)
        if len(ctx.PostBody())>0{
            err = json.Unmarshal(ctx.PostBody(), &request)
            helper.CheckErr(err)
            if request["token"] != nil {
                token = request["token"].(string)
            }
        }

        // если в теле запроса нет токена - ищем в хедере @todo возможно следует оставить один вариант передачи токена
        if token == "" {
            headerToken := string(ctx.Request.Header.Peek("Authorization"))
            if len(headerToken)>0 {
                token = strings.Replace(headerToken, "Bearer ", "", -1)
            } else {
                helper.ResponseUnauthorized(ctx)
                return
            }
        }

        // выходим если нет пользователя с token
        err = user.Authenticated(token)
        if err != nil {
            helper.ResponseUnauthorized(ctx)
            return
        }

        // все норм, пользователь есть.
        // добавим его в объект запроса
        ctx.SetUserValue("authUser", user)

        next(ctx)
    }
}

// handle middleware
func LogMiddleware(handle HandleFunc) HandleFunc{
    return func(ctx *fasthttp.RequestCtx){
        //fmt.Println("I AM LOG MW")
        handle(ctx)
    }
}

// делает "композицию" из переданных middleware
// возвращает ф-ю в которую передается handler роута
func MiddlewareCompose(middleWares ...MiddlewareFunc) MiddlewareFunc {

    return func( final HandleFunc) HandleFunc {
        return func(ctx *fasthttp.RequestCtx) {
            last := final
            for i := len(middleWares) - 1; i >= 0; i--  {
                last = middleWares[i](last)
            }
            last(ctx)
        }
    }
}

