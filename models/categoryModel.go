package models

import (
	"errors"
	gormPaging "github.com/Prabandham/paginator"
	"github.com/jinzhu/gorm"
	. "gophra/db"
)

type Category struct {
	ID        uint 		`gorm:"primary_key" json:"id"`
	Title     string 	`gorm:"type:varchar(100)" json:"title"`
	UserID	  uint      `gorm:"" json:"user_id"` // foreignkey на таблицу users
	User      User 		`gorm:"foreignkey:UserID" json:"-"`
	Lists	  []List	`json:"lists"`
	Status	  uint8		`json:"status"`
	CreatedAt	string  `gorm:"-" json:"created_at"`
	UpdatedAt	string  `gorm:"-" json:"updated_at"`
	// Active		bool `gorm:"default:true" json:"active"`
	*DbModel
}

// запись новой категории
func (c *Category) Store() error  {
	c.Status = 1
	err := DB.Create(&c).Error
	if err != nil {
		return errors.New("failed on adding record")
	}

	return nil
}

func (c *Category) GetUserCategory(userId uint) error {
	err := DB.Where("user_id = ?", userId).First(c)
	if err != nil {
		return err.Error
	}
	return nil
}

func (c *Category) GetLists( ) (*gormPaging.Data,error) {

    var (
        lists []List
        perPage = "2"
        page = "1"
    )

    db := DB.Model(c)
    if db.Error == gorm.ErrRecordNotFound {
        return nil, errors.New("record does not exist or access denied")
    }
    // выполняем запрос с учетом пагинации
    stmt := gormPaging.Paginator{DB: db, OrderBy: []string{"created_at desc"}, Page: page, PerPage: perPage}
    data := stmt.Paginate(&lists)
	return data, nil
}

