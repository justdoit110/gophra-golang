package handlers

import (
    "encoding/json"
    "fmt"
    "github.com/valyala/fasthttp"
    "gophra/helper"
    . "gophra/models"
    "gophra/types"
    "strconv"
)

// сохраняет/пишет в БД
func ListStore(ctx *fasthttp.RequestCtx)  {

    resp := &types.ResponseData{ Ctx: ctx}
    var (
        list = List{}
        user = User{}
    )

    err := json.Unmarshal(ctx.PostBody(), &list)
    helper.CheckErr(err)

    // валидация данных запроса
    if err := list.ValidateStoreReq(&list); err != nil {
       helper.Response400(resp, map[string]interface{}{"error": err} )
       return
    }

    // детектим текущего пользователя
    ok := user.GetAuthUser(ctx)
    if !ok {
        helper.Response400(resp, types.MapSs{"error": "can not resolve user"})
        return
    }

    // проверяем доступ пользователя к категории в которую добавляется список
    category := &Category{}
    err = category.GetUserCategory(user.ID)
    if err != nil {
        helper.Response400(resp, types.MapSs{"error": "category not found or current user is not its owner"})
        return
    }

    err = list.Store()
    if err != nil {
       resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {
       resp.SetData( types.MapSs{"success": "list created", "id": fmt.Sprint(list.ID) }, fasthttp.StatusOK )
    }

    helper.Response(resp)

}

// достает/показывает объект List
func ListShow(ctx *fasthttp.RequestCtx) {

    resp := &types.ResponseData{ Ctx: ctx}
    var user = User{}


    if ok := user.GetAuthUser(ctx); !ok {
        helper.Response400(resp, "can not resolve user")
        return
    }

    // получаем id из url (/api/list/123), оно в string
    listId := ctx.UserValue("listId")

    id, err := strconv.Atoi(listId.(string))
    // вернем 400, если не смогли конвертировать listId в int
    if err != nil {
        helper.Response400(resp, types.MapSs{})
        return
    }

    list := &List{}
    // достаем List и родительскую Category
    err = list.GetUserList(user.ID, uint(id))
    if err != nil {
        resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {
        resp.SetData( list )
    }

    helper.Response(resp)

}

// достает фразы из списка
func ListShowPhrases(ctx *fasthttp.RequestCtx) {

    resp := &types.ResponseData{ Ctx: ctx}
    var (
        user = User{}
        pageNum = "1"
    )

    if ok := user.GetAuthUser(ctx); !ok {
        helper.Response400(resp, "can not resolve user")
        return
    }

    // получаем id из url (/api/list/123), оно в string
    listId := ctx.UserValue("listId")

    id, err := strconv.Atoi(listId.(string))
    // вернем 400, если не смогли конвертировать listId в int
    if err != nil {
        helper.Response400(resp, types.MapSs{})
        return
    }

    // пришел парамет page (номер страницы)
    if page := ctx.QueryArgs().Peek("page"); page != nil {
        // номер страницы это число
        if _,err := strconv.Atoi(string(page)); err == nil {
            pageNum = string(page)
        }
    }

    list := &List{}
    // достаем List и родительскую Category
    phrases, err := list.GetUserListPhrases(user.ID, uint(id), pageNum)
    if err != nil {
        resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {

        resp.SetData( phrases, fasthttp.StatusOK )

    }

    helper.Response(resp)

}