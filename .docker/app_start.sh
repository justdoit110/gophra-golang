#!/bin/bash

app_port=8080
echo -e "get packages ... \n"
cd $GOPATH/src/gophra/ && go get -d ./...
echo -e "build an app ... \n"
go build -o /go/bin/gophra

if [ ! -z "$APP_PORT" ]
then
    app_port=$APP_PORT
fi
echo -e "run app ... \n"
/go/bin/gophra -app_port $app_port
