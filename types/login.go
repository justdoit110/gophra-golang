package types

import "github.com/badoux/checkmail"

type LoginReq struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

func (r *LoginReq) validate() MapSs  {

	errs := make(MapSs)

	err := checkmail.ValidateFormat(r.Email)
	if err != nil {
		if len(r.Email)==0{
			errs["email"] = "email is empty"
		} else {
			errs["email"] = "incorrect email format"
		}
	}

	if len(r.Password)==0 {
		errs["password"] = "password is empty"

	}
	return errs
}
