package handlers

import (
    "github.com/valyala/fasthttp"
    "gophra/helper"
    . "gophra/models"
    "gophra/types"
)

func UserMenu(ctx *fasthttp.RequestCtx)  {

    resp := &types.ResponseData{Ctx: ctx}
    var user = User{}

    if ok := user.GetAuthUser(ctx); !ok {
        helper.Response400(resp, "can not resolve user")
        return
    }


    categories, err := GetUserCategories(user.ID)
    if err != nil {
        resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {

        resp.SetData( categories, fasthttp.StatusOK )
    }


    helper.Response(resp)
}
