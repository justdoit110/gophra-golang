package models

import (
    "errors"
    "fmt"
    "github.com/thedevsaddam/govalidator"
    . "gophra/db"
    "strings"
)

type Phrase struct {
    ID          uint    `gorm:"primary_key" json:"id"`
    Phrase       string  `gorm:"type:varchar(255)" json:"phrase"`
    Translate       string  `gorm:"type:varchar(255)" json:"translate"`
    ListID  uint  `gorm:"type:integer" json:"list_id"` // foreignkey на таблицу categories
    List    List    `gorm:"foreignkey:ListID" json:"-"`
    CreatedAt	string `gorm:"-" json:"created_at"`
    UpdatedAt	string `gorm:"-" json:"updated_at"`
    // Active		bool `gorm:"default:true" json:"active"`
    *DbModel
}

func (p *Phrase) Store() error {
    err := DB.Create(&p).Error
    if err != nil {
        fmt.Printf("\nError when store category: %v\n",err)
        return errors.New("error adding record")
    }
    return nil
}

// валидация данных/полей в запросе
func (p *Phrase) ValidateStoreReq(phrase *Phrase) interface{} {

   rules := govalidator.MapData{
       "phrase": []string{"required", "min:3", "max:255"},
       "translate": []string{"required", "min:3", "max:255"},
       //"category_id":    []string{"required", "numeric"},
   }

   opts := govalidator.Options{
       Data:  phrase,
       Rules: rules,
       RequiredDefault: true,
   }

   v := govalidator.New(opts)

   if err := v.ValidateStruct(); len(err) > 0 {
       return err
   }

  return nil
}

// для массового добавления записей
func (p *Phrase) BulkInsert(unsavedRows []*Phrase) error {
    var tableName = "phrases"
    valueStrings := make([]string, 0, len(unsavedRows))
    valueArgs := make([]interface{}, 0, len(unsavedRows) * 3)
    i := 0
    for _, post := range unsavedRows {
        valueStrings = append(valueStrings, fmt.Sprintf("($%d, $%d, $%d)", i*3+1, i*3+2, i*3+3))
        valueArgs = append(valueArgs, post.Phrase)
        valueArgs = append(valueArgs, post.Translate)
        valueArgs = append(valueArgs, post.ListID)
        i++
    }
    stmt := fmt.Sprintf("INSERT INTO "+tableName+" (phrase, translate, list_id) VALUES %s", strings.Join(valueStrings, ","))
    err := DB.Exec(stmt, valueArgs...)
    return err.Error
}


