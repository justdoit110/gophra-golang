package types

import (
	"github.com/badoux/checkmail"
	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
	"net/url"
)

// map {string: string}
type MapSs map[string]string

type HandleFunc = func( *fasthttp.RequestCtx)

// тип ф-и middleware
type MiddlewareFunc = func( HandleFunc) func( *fasthttp.RequestCtx)

type FHRouter = *fasthttprouter.Router

// Route Group Routes
type RgRoute struct {
	Method 	string
	Path 	string
	Handler HandleFunc
}


/**
структура запроса на регистрацию
 */
type RegisterReq struct {
	Name string `json:"name"`
	Email string `json:"email"`
	Password string `json:"password"`
}

/**
 * Валидация запроса на рег-ю
 */
func (r *RegisterReq) validate() url.Values {

	errs := url.Values{}

	err := checkmail.ValidateFormat(r.Email)
	if err != nil {
		if len(r.Email)==0{
			errs.Add("email", "empty email")
		} else {
			errs.Add("email", "incorrect email format")
		}
	}

	if len(r.Password)<6 {
		errs.Add("password", "password min length 6 chars")

	}
	return errs
}

//
func (r *RegisterReq) ValidateMk() map[string]string {

	errs := make(map[string]string)

	err := checkmail.ValidateFormat(r.Email)
	if err != nil {
		if len(r.Email)==0{
			errs["email"] = "empty email"
		} else {
			errs["email"] = "incorrect email format"
		}
	}

	if len(r.Password)<6 {
		errs["password"] = "password min length 6 chars"

	}
	return errs
}

type ResponseData struct {
	Ctx *fasthttp.RequestCtx
	//Data map[string]string
	Data interface{}
	Status int
}

func (r *ResponseData) SetData(data interface{}, status ...int)  {
	r.Data = data
	// если не указать то по дефолту статус будет 200 ( в helpers.Response() )
	if len(status)>0 {
		r.Status = status[0]
	}
}
