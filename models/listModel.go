package models

import (
    "errors"
    "fmt"
    gormPaging "github.com/Prabandham/paginator"
    "github.com/jinzhu/gorm"
    "github.com/thedevsaddam/govalidator"
    . "gophra/db"
)

type List struct {
    ID          uint    `gorm:"primary_key" json:"id"`
    Title       string  `gorm:"type:varchar(100)" json:"title"`
    CategoryID  int  `gorm:"type:integer" json:"category_id"` // foreignkey на таблицу categories
    Category    Category    `gorm:"foreignkey:CategoryID" json:"-"`
    Phrases     []Phrase	`json:"phrases"`
    Status	  uint8		`json:"status"`
    CreatedAt	string `gorm:"-" json:"created_at"`
    UpdatedAt	string `gorm:"-" json:"updated_at"`
    // Active		bool `gorm:"default:true" json:"active"`
    *DbModel
}

// создание/сохранение в БД
func (l *List) Store() error {
    // при создании/инициализации структуры, поле Status будет = 0 (по-дефолту для типов int)
    // поэтому поставим дефолтное значение = 1
    l.Status = 1
    err := DB.Create(&l).Error
    if err != nil {
        fmt.Printf("\nError when store category: %#v\n", l)
        return errors.New("failed when adding record")
    }

    return nil
}


// Достает List(listId) конкретного пользователя (userId)
func (l *List) GetUserList(userId uint, listId uint) error {
    err := DB.Preload("Category", "user_id = ?", fmt.Sprint(userId) ).First(&l, listId)
    if err.Error == gorm.ErrRecordNotFound {
        return errors.New("record does not exist or access denied")
    }
    return nil
}

// Достает фразы из listId конкретного пользователя userId
func (l *List) GetUserListPhrases(userId uint, listId uint, pageNum ...string) (*gormPaging.Data, error) {
    var (
        phrases []*Phrase
        perPage = "15"
        page = "1"
    )

    if len(pageNum)>0 {
        page = pageNum[0]
    }

    // подготовка запроса
    db := DB.Preload("List.Category", "user_id = ?", fmt.Sprint(userId)).
            Where("list_id = ?", fmt.Sprint(listId))
    if db.Error == gorm.ErrRecordNotFound {
        return nil, errors.New("record does not exist or access denied")
    }
    // выполняем запрос с учетом пагинации
    stmt := gormPaging.Paginator{DB: db, OrderBy: []string{"created_at,id desc"}, Page: page, PerPage: perPage}
    data := stmt.Paginate(&phrases)

    return data, nil
}

func (l *List) ValidateStoreReq(listIn *List) interface{} {

    rules := govalidator.MapData{
        "title": []string{"required", "min:3", "max:70"},
        "category_id":    []string{"required", "numeric"},
    }

    opts := govalidator.Options{
        Data:  listIn,
        Rules: rules,
        RequiredDefault: true,
    }

    v := govalidator.New(opts)

    if err := v.ValidateStruct(); len(err) > 0 {
        return err
    }

   return nil
}


