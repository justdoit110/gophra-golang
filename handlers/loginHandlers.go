package handlers

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	. "gophra/models"
	"gophra/helper"
	"gophra/types"
)

// авторизация
func Login(ctx *fasthttp.RequestCtx)  {

	resp := &types.ResponseData{ Ctx: ctx}
	var user = &User{}
	var req types.MapSs

	err := json.Unmarshal(ctx.PostBody(), &req)
	helper.CheckErr(err)

	res := user.Authenticate(req)
	if res == false {
		resp.SetData( types.MapSs{"error": "user not found (wrong email or password)"}, fasthttp.StatusBadRequest )
	} else {
		resp.SetData( types.MapSs{"success": "authorized", "token": res.(string)}, fasthttp.StatusOK )
	}
	helper.Response(resp)
}

