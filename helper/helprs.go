package helper

import (
    "encoding/json"
    "fmt"
    "github.com/valyala/fasthttp"
    "golang.org/x/crypto/bcrypt"
    . "gophra/types"
    "log"
    "math/rand"
    "time"
)


func CheckErr(err error) {
	if err != nil {
		//log.Fatal("ERROR: ", err.Error())
		log.Println("ERROR: ", err.Error())
	}
}

/**
 выполняет/возвращает "ответ"
 */
func Response(data *ResponseData)  {

    var status = 200
	if data.Status != 0 {
        status = data.Status
    }

	data.Ctx.SetStatusCode( status )
	data.Ctx = SetCORS(data.Ctx)
	// вернем json, если был запрос в json формате
	if wantJsonResponse(data.Ctx) {
		jsonResponse(data.Ctx, data.Data)
		return
	}

	fmt.Fprintf(data.Ctx, "%v", data.Data)

}

/**
 * ответ 400
 */
func Response400(respObj *ResponseData, payload interface{}){
	respObj.SetData( payload, fasthttp.StatusBadRequest )
	Response(respObj)
}

/**
 * ответ 403
 */
func Response403(respObj *ResponseData){
    respObj.SetData( MapSs{}, fasthttp.StatusBadRequest )
    Response(respObj)
}

/**
 * ответ в формате json
 */
func jsonResponse(ctx *fasthttp.RequestCtx, resp interface{})  {
	ctx.Response.Header.SetContentType("application/json")
	if err := json.NewEncoder(ctx).Encode(resp); err != nil {
		ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}

/**
 * заголовок запроса Content-Type == "application/json" ?
 */
func wantJsonResponse(ctx *fasthttp.RequestCtx) bool {
	return string(ctx.Request.Header.ContentType()[:]) == "application/json"

}

func ResponseInternalError(data *ResponseData)  {
	data.Status = fasthttp.StatusInternalServerError
	data.Data = MapSs{"500":"Internal Server Error"}
	Response(data)
}

/**
 generate psswd hash
 */
func PwdHash(pwd string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.MinCost)
	if err != nil {
		log.Fatal(err.Error())
	}
	return string(hash)
}

// HTTP ответ 401
func ResponseUnauthorized(ctx *fasthttp.RequestCtx)  {
	data := &ResponseData{}
	data.Ctx = ctx
	data.Status = fasthttp.StatusUnauthorized
	data.Data = MapSs{"error":"unauthorized 401"}
	Response(data)
}

// генератор случайной строки
func RandomString(n int) string {

	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-"
	const (
		letterIdxBits = 6                    // 6 bits to represent a letter index
		letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
		letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
	)
	var src = rand.NewSource(time.Now().UnixNano())

	b := make([]byte, n)
	l := len(letterBytes)

	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {

		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < l {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)

}

//func Test(ctx *fasthttp.RequestCtx) error {
//
//
//    Response400(ctx, )
//}

func SetCORS(ctx *fasthttp.RequestCtx) *fasthttp.RequestCtx  {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	return ctx
}