package handlers

import (
    "encoding/json"
    "fmt"
    "github.com/valyala/fasthttp"
    "gophra/helper"
    . "gophra/models"
    "gophra/types"
    "strconv"
)

// handler запроса на добавление категории
func CategoryStore(ctx *fasthttp.RequestCtx)  {

	resp := &types.ResponseData{ Ctx: ctx}
	var (
	    user = User{}
	    //request types.MapSs
        category = Category{}
    )

	// конвертируем запрос в структуру request
	err := json.Unmarshal(ctx.PostBody(), &category)
	helper.CheckErr(err)

	// валидируем длинну заголовка категории
	if len(category.Title) < 3 {
		helper.Response400(resp, types.MapSs{"error": "min length for 'title' = 3 char"})
		return
	}

	// проверяем "наличие" пользователя

    if ok := user.GetAuthUser(ctx); !ok {
        helper.Response400(resp, types.MapSs{"error": "can not resolve user"})
        return
    }

	// пишем категорию в БД
	category.UserID = user.ID
	err = category.Store()
	if err != nil {
		resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
	} else {
		resp.SetData( types.MapSs{"success": "category created", "id": fmt.Sprint(category.ID) })
	}

	helper.Response(resp)

}

// список lists конкретной категории
func CategoryShowLists(ctx *fasthttp.RequestCtx) {

    resp := &types.ResponseData{ Ctx: ctx}
    var user = User{}


    if ok := user.GetAuthUser(ctx); !ok {
        helper.Response400(resp, "can not resolve user")
        return
    }

    // получаем id из url (/api/category/123), оно в string
    id := ctx.UserValue("categoryId")

    categoryId, err := strconv.Atoi(id.(string))
    // вернем 400, если не смогли конвертировать categoryId в int
    if err != nil {
        helper.Response400(resp, types.MapSs{})
        return
    }

    category := &Category{}
    category.UserID = user.ID
    category.ID = uint(categoryId)
    lists, err := category.GetLists()
    if err != nil {
        resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {

        resp.SetData( lists, fasthttp.StatusOK )
    }


    helper.Response(resp)

}

func CategoryShow(ctx *fasthttp.RequestCtx) {

	resp := &types.ResponseData{ Ctx: ctx}
	var user = User{}


    if ok := user.GetAuthUser(ctx); !ok {
        //ctx.Response.Write(user)
        helper.Response400(resp, "can not resolve user")
        return
    }

    // получаем id из url (/api/category/123), оно в string
    categoryId := ctx.UserValue("categoryId")

    id, err := strconv.Atoi(categoryId.(string))
    // вернем 400, если не смогли конвертировать categoryId в int
    if err != nil {
        helper.Response400(resp, types.MapSs{})
        return
    }

    category := &Category{}
    err = category.GetById(uint(id), category)
    if err != nil {
        resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {

        // проверка что
        if category.UserID == user.ID {

            resp.SetData( category, fasthttp.StatusOK )

        } else {
            helper.Response403(resp)
            return
        }
    }


    helper.Response(resp)

}

