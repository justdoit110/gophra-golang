package main

import (
	"fmt"
	"github.com/valyala/fasthttp"
)



func Index(ctx *fasthttp.RequestCtx) {
	fmt.Fprint(ctx, "Welcome!\n")
}

func Hello(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(ctx, "hello, %s!\n", ctx.UserValue("name"))
}


// разрешаем CORS (в запросах OPTIONS)
func OptionReqHandler(ctx *fasthttp.RequestCtx)  {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	//ctx.Response.Header.Set("Content-Type", "application/json")
	//json.NewEncoder(ctx).Encode(map[string]string{"var":"value"})
}




//func validateRegisterRequest(regObj *models.User, responseData *ResponseData)  {
//
//	err := checkmail.ValidateFormat(regObj.Email)
//	if err != nil {
//		if len(regObj.Email)==0{
//			responseData.Data["email"] = "empty email"
//		} else {
//			responseData.Data["email"] = "incorrect email format"
//		}
//	}
//
//	if len(regObj.Password)<6 {
//		responseData.Data["password"] = "password min length 6 chars"
//
//	}
//}