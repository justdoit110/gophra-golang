package handlers

import (
    "encoding/json"
    "fmt"
    "github.com/valyala/fasthttp"
    "gophra/helper"
    . "gophra/models"
    "gophra/types"
    "strconv"
    "strings"
)

func PhraseStore(ctx *fasthttp.RequestCtx)  {

    resp := &types.ResponseData{ Ctx: ctx}
    var (
        phrase = Phrase{}
        user = User{}
    )

    err := json.Unmarshal(ctx.PostBody(), &phrase)
    helper.CheckErr(err)

    // валидация данных запроса
    if err := phrase.ValidateStoreReq(&phrase); err != nil {
       helper.Response400(resp, err)
       return
    }

    // детектим текущего пользователя
    if ok := user.GetAuthUser(ctx); !ok {
        helper.Response400(resp, types.MapSs{"error": "can not resolve user"})
        return
    }

    // получаем id из url (/api/list/123), оно в string
    listId := ctx.UserValue("listId")

    id, err := strconv.Atoi(listId.(string))
    // вернем 400, если не смогли конвертировать listId в int
    if err != nil {
        helper.Response400(resp, types.MapSs{"error": "wrong/bad listId"})
        return
    }

    list := &List{}
    // достаем List и родительскую Category
    err = list.GetUserList(user.ID, uint(id))
    if err != nil {
        helper.Response400(resp, types.MapSs{"error": err.Error()})
        return
    }

    // указываем id списка в который добавится "фраза"
    phrase.ListID = uint(id)

    err = phrase.Store()
    if err != nil {
        resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {
        resp.SetData( types.MapSs{"success": "a phrase created", "id": fmt.Sprint(phrase.ID) } )
    }

    helper.Response(resp)

}

// сохранить "пачку" фраза-перевод
func PhrasesStore(ctx *fasthttp.RequestCtx)  {

    resp := &types.ResponseData{ Ctx: ctx}
    var (
        user = User{}
        massPhrases types.MapSs
    )

    err := json.Unmarshal(ctx.PostBody(), &massPhrases)
    helper.CheckErr(err)

    // разбиваем входящую строку по EOL, получим массив строк где строки 0,2,4,... это фразы
    // а строки 1,3,5,... это переводы фраз
    phraseSlice := strings.Split( strings.Trim(massPhrases["phrases"], "\n"), "\n")
    if len(phraseSlice)<2 || len(phraseSlice)%2 != 0 {
        helper.Response400(resp, types.MapSs{"error": "looks like a phrases string is incorrect"})
        return
    }

    // детектим текущего пользователя
    user.GetAuthUser(ctx)

    // получаем id из url (/api/list/123), оно в string
    listId := ctx.UserValue("listId")

    id, err := strconv.Atoi(listId.(string))
    // вернем 400, если не смогли конвертировать listId в int
    if err != nil {
        helper.Response400(resp, types.MapSs{"error": "wrong/bad listId"})
        return
    }

    list := &List{}
    // достаем List и родительскую Category
    err = list.GetUserList(user.ID, uint(id))
    if err != nil {
        helper.Response400(resp, types.MapSs{"error": err.Error()})
        return
    }

    // формируем массив Phrase
    var phrases []*Phrase
    for i:=len(phraseSlice)-1; i>=0; i-=2 {
        phrases = append(phrases, &Phrase{
            ListID:    uint(id),
            Phrase:    strings.TrimSpace(phraseSlice[i-1]),
            Translate: strings.TrimSpace(phraseSlice[i]),
        })
    }

    // пишем в БД всю "пачку" фраз
    err = (&Phrase{}).BulkInsert(phrases)

    if err != nil {
        resp.SetData( types.MapSs{"error": err.Error()}, fasthttp.StatusInternalServerError )
    } else {
        resp.SetData( types.MapSs{"success": "phrases created" } )
    }

    helper.Response(resp)

}


