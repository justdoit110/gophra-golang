#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

	GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_USER TO $POSTGRES_DB;

-- auto-generated definition
create table users
(
  id         serial                              not null
    constraint users_pk
    primary key,
  name       varchar(255)                        not null,
  email      varchar(255)                        not null,
  password   varchar(255)                        not null,
  token      varchar(60),
  active     boolean   default true,
  created_at timestamp default CURRENT_TIMESTAMP not null,
  updated_at timestamp default CURRENT_TIMESTAMP
);

alter table users
  owner to gophra;

create unique index users_id_uindex
  on users (id);

-- auto-generated definition
create table categories
(
  id         serial       not null,
  title      varchar(255) not null,
  user_id    integer      not null,
  status     smallint  default 1,
  created_at timestamp default CURRENT_TIMESTAMP,
  updated_at timestamp default CURRENT_TIMESTAMP
);

comment on column categories.status
is '1-active
2-off
0-removed';

alter table categories
  owner to gophra;

-- auto-generated definition
create table lists
(
  id          serial       not null,
  title       varchar(255) not null,
  category_id integer      not null,
  status      smallint  default 1,
  created_at  timestamp default CURRENT_TIMESTAMP,
  updated_at  timestamp default CURRENT_TIMESTAMP
);

comment on column lists.status
is '1-active
2-off
0-removed';

alter table lists
  owner to gophra;

-- auto-generated definition
create table phrases
(
  id         serial       not null,
  phrase     varchar(255) not null,
  translate  varchar(255) not null,
  list_id    integer      not null,
  status     smallint  default 1,
  created_at timestamp default CURRENT_TIMESTAMP,
  updated_at timestamp default CURRENT_TIMESTAMP
);

comment on column phrases.status
is '1-active
2-off
0-removed';

alter table phrases
  owner to gophra;

EOSQL